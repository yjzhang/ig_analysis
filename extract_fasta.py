import sys

from Bio.SeqIO import FastaIO

fasta_filename = sys.argv[1]
start_range = int(sys.argv[2])
end_range = int(sys.argv[3])

fasta_in = open(fasta_filename)
fasta = FastaIO.FastaIterator(fasta_in)
for seq_record in fasta:
	print(str(seq_record.seq[start_range:end_range]))
