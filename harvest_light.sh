#!/bin/sh

# Takes all the "final analysis products" from the folders - 

BASE_DIR=/Users/muellerlab/Desktop/jack_immunoglobulin

mkdir $BASE_DIR/mayo_light_results

for folder in $BASE_DIR/mayo_data_light_out/*; do
		name=`basename $folder`
		new_out=$BASE_DIR/mayo_light_results/$name
		mkdir $new_out
		echo $folder
		cd $folder
		cp $folder/final_output_10.fasta $new_out/
		cp -R $folder/output $new_out/
		cp $folder/IMGT_V-QUEST.html $new_out/
		cp -R $folder/IMGT_V-QUEST_files $new_out/
		cp $folder/best.fasta $new_out/
		cp $folder/vdj_igmt.fasta $new_out/
done
