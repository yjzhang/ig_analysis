#!/bin/sh

FILES_DIR=$1
OUTPUT_DIR=$2

for file in `ls $FILES_DIR/*.bam`; do
		filename=`basename $file`
		echo $filename
		sh igl_pipeline.sh $file $OUTPUT_DIR/$filename
done

#ls ../mayo_data/*.bam | xargs -n1 basename | xargs -n1 ./igl_pipeline.sh
#ls ../mayo_data/*.bam | tail -n+7 | xargs -n1 basename | xargs -n1 ./test_script.sh

