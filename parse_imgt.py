import re
import os
import sys
import os.path

"""
This is supposed to extract the V-J region from IMGT's html output.
"""

def extract_vj_seq(data, cdr_length_string):
	#cdr_length_string = cdr_length_string.replace('.', '\\.').replace('[', '\\[').replace(']', '\\]')
	#fr_length_string = fr_length_string.replace('.', '\\.').replace('[', '\\[').replace(']', '\\]')
	#pattern = re.compile('CDR_length\\=\\"' + cdr_length_string)
	#pattern2 = re.compile('FR_length\\=\\"' + fr_length_string)
	new_data = data.split('CDR_length="' + cdr_length_string)[1]
	if len(new_data) > 2:
		print('WARNING: possible errors due to multiple sequences')
	pattern = re.compile('[agct]+')
	seq = []
	for line in new_data.split('\n')[3:]:
		print(line)
		if line.rfind('translation') > 0:
			break
		else:
			seq.append(line.strip())
	return '\n'.join(seq)

if __name__ == '__main__':
	basedir = '/Users/muellerlab/Desktop/jack_immunoglobulin/mayo_data_light_out'
	cdrs_file = open(os.path.join(basedir, 'cdrs.txt'))
	new_file = open(os.path.join(basedir, 'all_vjs.txt'), 'w')
	cdrs = {}
	for c in cdrs_file.readlines():
		c2 = c.split('\t')
		cdrs[c2[0]] = c2[1].strip()
	cdrs_file.close()
	pattern = re.compile('C[0-9]{2}')
	for root, dirs, files in os.walk(basedir):
		foldername = os.path.basename(root)
		if pattern.match(foldername):
			print(foldername)
			try:
				datafile = open(os.path.join(root, 'IMGT_V-QUEST.html'))
				data = datafile.read()
				cdr_string = cdrs[foldername]
				seq = extract_vj_seq(data, cdr_string)
				new_fasta = open(os.path.join(root, 'vj_imgt.fasta'), 'w')
				new_fasta.write('>' + foldername + '\n')
				new_fasta.write(seq)
				new_fasta.close()
				new_file.write('>'+foldername+'\n')
				new_file.write(seq)
				new_file.write('\n')
				datafile.close()
			except IOError:
				print("Error: unable to open file")
	new_file.close()
