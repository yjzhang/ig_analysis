#!/bin/bash

# script to run pipeline on stuff

# input bam file name as first argument

INPUT_FILE_NAME=`basename $1`
OUTPUT_DIR_FOLDER=${INPUT_FILE_NAME:14:3}


BASE_DIR=/Users/muellerlab/Desktop/jack_immunoglobulin
#OUTPUT_DIR=$BASE_DIR/mayo_data_out/$OUTPUT_DIR_FOLDER
OUTPUT_DIR=$2
#INPUT_BAM=$BASE_DIR/mayo_data/$INPUT_FILE_NAME 
INPUT_BAM=$1
READS_1=r1.fastq
READS_2=r2.fastq
READS_1_A=r1_adapter.fastq
READS_2_A=r2_adapter.fastq
READS_1_T=r1_trimmed.fastq
READS_2_T=r2_trimmed.fastq

echo Input file: $INPUT_BAM
echo Writing to $OUTPUT_DIR

mkdir -p $OUTPUT_DIR
cp ./*.py $OUTPUT_DIR/
cd $OUTPUT_DIR

samtools sort -@ 4 -O bam -T temp -n $INPUT_BAM > input_sorted.bam
bedtools bamtofastq -i input_sorted.bam -fq $READS_1 -fq2 $READS_2

# 1. preprocessing

# 1.1. remove illumina adapter contamination using scythe (however, according to fastqc, we don't have adapter contamination)

scythe -q sanger -a $BASE_DIR/adapters.fasta -o $READS_1_A $READS_1
scythe -q sanger -a $BASE_DIR/adapters.fasta -o $READS_2_A $READS_2

# 1.2. quality-trim reads using sickle - settings: no 5' trimming, default quality/length thresholds

sickle pe -x -t sanger -s trimmed.fastq -f $READS_1_A -r $READS_2_A -o $READS_1_T -p $READS_2_T
rm $READS_1_A $READS_2_A

# mapping 1. extract region of interest (IGH locus) from hg19

#bedtools getfasta -fi ../hg19.fa -bed ../igh@.bed -fo ../genome/igh.fa

# mapping 2. use STAR or bowtie2 to map stuff to region of interest

# first, STAR has to generate an index.

#STAR --runThreadN 4 --genomeSAindexNbases 9 --runMode genomeGenerate --genomeDir ../genome/ --genomeFastaFiles ../igh.fa

# next, do the alignment using STAR.

#STAR --runThreadN 4 --genomeDir ../genome/ --readFilesIn SRR1814049_1_trimmed.fastq SRR1814049_2_trimmed.fastq 

# STAR didn't work for some reason... try bowtie2?

bowtie2-build $BASE_DIR/igh.fa $BASE_DIR/igh_bowtie_index/index

bowtie2 --local -p 4 -x $BASE_DIR/igh_bowtie_index/index -1 $READS_1_T -2 $READS_2_T -S bowtie_align.sam

# 2. de novo transcriptome assembly using Trinity

# take all the mapped bowtie2 reads somehow

# sort
samtools sort -O bam -T temp bowtie_align.sam > align_sorted.bam

# index
samtools index -b align_sorted.bam

### filter- get all reads that align at least once - from https://www.biostars.org/p/56246/
samtools view -b -F 4 -f 8 align_sorted.bam > onlyThisEndMapped.bam
samtools view -b -F 8 -f 4 align_sorted.bam > onlyThatEndMapped.bam
samtools view -b -F12 align_sorted.bam > bothEndsMapped.bam
samtools merge merged.bam onlyThisEndMapped.bam onlyThatEndMapped.bam bothEndsMapped.bam
samtools sort -@ 4 -O bam -T temp -n merged.bam > align_filtered.bam
rm onlyThisEndMapped.bam onlyThatEndMapped.bam bothEndsMapped.bam merged.bam bowtie_align.sam align_sorted.bam

# get fastq from bam file
bedtools bamtofastq -i align_filtered.bam -fq align1.fastq -fq2 align2.fastq

# Use Trinity using both fastq files
$BASE_DIR/trinityrnaseq-2.0.6/Trinity --bypass_java_version_check --seqType fq --max_memory 8G --left align1.fastq --right align2.fastq --CPU 4

# delete trinity result
cp trinity_out_dir/Trinity.fasta ./
rm -rf trinity_out_dir

# Use BLAST to identify putative transcripts with homology to the igv loci?
# file is igmt.fasta, output xml
makeblastdb -in $BASE_DIR/ighv_ref.fasta -dbtype nucl
blastn -outfmt 5 -db $BASE_DIR/ighv_ref.fasta -query Trinity.fasta -word_size 21 > blast.out

# use that one python script to extract sequences
python parse_blast.py

# use bowtie2/eXpress to quantify expression for the new transcriptome
#bowtie2-build extracted_seqs.fasta new_transcripts
#bowtie2 --local -p 4 -x new_transcripts -1 $READS_1_T -2 $READS_2_T -S new_bowtie_align.sam
# TODO: shuffle alignments:
#express extracted_seqs.fasta new_bowtie_align.sam

# Use kallisto to quantify expression?
kallisto index -i transcripts.idx extracted_seqs.fasta
kallisto quant -i transcripts.idx -o output -b 100 $READS_1_T $READS_2_T

# express's output file is called 'results.xprs'.
# use a python/pandas script to parse, sort, extract the relevant fasta (one with the greatest tpm?).
python parse_kallisto.py

# the final output file is called final_output_10.fasta

# TODO: identify constant region
makeblastdb -in $BASE_DIR/ighc_ref.fasta -dbtype nucl
blastn -outfmt 5 -db $BASE_DIR/ighc_ref.fasta -query final_output_10.fasta -word_size 21 > constant_blast.out
# TODO: write script to find constant region

touch igblast_best_result_$OUTPUT_DIR_FOLDER.txt
