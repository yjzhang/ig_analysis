#!/bin/sh

BASE_DIR=/Users/muellerlab/Desktop/jack_immunoglobulin

makeblastdb -in $BASE_DIR/ighc_ref.fasta -dbtype nucl

for folder in $BASE_DIR/mayo_heavy_results/*; do
	if [ -f $folder/final_output_10.fasta ]; then
			blastn -outfmt 5 -db $BASE_DIR/ighc_ref.fasta -query $folder/final_output_10.fasta -word_size 21 > $folder/constant_blast.out
			python parse_blast_constant.py $folder/constant_blast.out $folder/constant_results.txt
	fi
	blastn -outfmt 5 -db $BASE_DIR/ighc_ref.fasta -query $folder/best.fasta -word_size 21 > $folder/constant_blast_best.out
	python parse_blast_constant.py $folder/constant_blast_best.out $folder/constant_results_best.txt
done
