from ete2 import Tree, TreeStyle
import pandas as pd

def igl_tree_draw():
	t = Tree('igl_phylogeny/tree1.txt')
	circular_style = TreeStyle()
	circular_style.mode = 'c'
	data = pd.read_table('data.tsv')
	data.index = data['Sequence ID']
	leaves = t.get_leaves()
	for leaf in leaves:
		light_chain = data.loc[leaf.name]['Light V'].split(' ')[1]
		leaf.name = leaf.name + ' - ' + light_chain
		light_chain_basic = light_chain.split('-')[0]
	# TODO: common ancestors? set color of all IGKV, etc.
	t.show(tree_style=circular_style)

def igh_tree_draw():
	t = Tree('igh_all_tree1.txt')
	circular_style = TreeStyle()
	circular_style.mode = 'c'
	data = pd.read_table('data.tsv')
	data.index = data['Sequence ID']
	leaves = t.get_leaves()
	for leaf in leaves:
		heavy_chain = data.loc[leaf.name]['IGHV'].split(' ')[1]
		leaf.name = leaf.name + ' - ' + heavy_chain
		heavy_chain_basic = heavy_chain.split('-')[0]
	# TODO: common ancestors? set color of all IGKV, etc.
	t.show(tree_style=circular_style)
