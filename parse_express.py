import pandas as pd
from Bio.SeqIO import FastaIO

if __name__ == '__main__':
	f = open('extracted_seqs.fasta')
	fasta_out = open('final_output_10.fasta', 'w')
	writer = FastaIO.FastaWriter(fasta_out)
	writer.write_header()
	fasta = FastaIO.FastaIterator(f)
	data = pd.read_table('results.xprs')
	data2 = data.sort('tpm', ascending=False)
	print(data.sort('fpkm', ascending=False).head())
	ids_to_extract = set(data2.target_id.iloc[0:10])
	for seq in fasta:
		if seq.id in ids_to_extract:
			print(seq)
			writer.write_record(seq)
	writer.write_footer()
	f.close()
	fasta_out.close()
