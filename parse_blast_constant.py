import sys
from Bio.Blast import NCBIXML
from Bio.SeqIO import FastaIO

def extract_matches(blast_filename, output_filename):
	"""
	Extracts the top match for each record.
	"""
	results = open(blast_filename)
	output = open(output_filename, 'w')
	try:
			blast_records = NCBIXML.parse(results)
			for record in blast_records:
				output_name = record.query.split(' ')[0]
				output.write(output_name)
				if record.alignments:
					output.write('\t')
					alignment = record.alignments[0]
					title = alignment.title.split('|')[3]
					output.write(title)
					output.write('\t')
					output.write(str(alignment.length))
					output.write('\t')
					output.write(str(alignment.hsps[0].identities))
					print(alignment.title)
					output.write('\n')
				else:
					output.write('\tNo constant region matches found\n')
	except:
			output.write('Error: failed to read blast results\n')
	results.close()
	output.close()

if __name__ == '__main__':
	blast_filename = sys.argv[1]
	output_filename = sys.argv[2]
	extract_matches(blast_filename, output_filename)
