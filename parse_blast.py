from Bio.Blast import NCBIXML
from Bio.SeqIO import FastaIO

def extract_seqs(blast_filename, fasta_filename):
	results = open(blast_filename)
	blast_records = NCBIXML.parse(results)
	f = open(fasta_filename)
	fasta = FastaIO.FastaIterator(f)
	ids_to_extract = set()
	for record in blast_records:
		if record.alignments:
			ids_to_extract.add(record.query)
	for seq_record in fasta:
		if seq_record.description in ids_to_extract:
			print(seq_record)
			yield seq_record
	results.close()
	f.close()

if __name__ == '__main__':
	fasta_out = open('extracted_seqs.fasta', 'w')
	writer = FastaIO.FastaWriter(fasta_out)
	writer.write_header()
	for seq in extract_seqs('blast.out', 'Trinity.fasta'):
		print(seq.id)
		writer.write_record(seq)	
	writer.write_footer()
	fasta_out.close()
